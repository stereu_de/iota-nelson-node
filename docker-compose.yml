version: "2"

services:

  iri:
    image: iotaledger/iri:latest
    container_name: iota_iri
    hostname: iri
    restart: unless-stopped
    volumes:
      - ./volumes/iri/iri.ini:/iri/conf/iri.ini:ro
      - ./volumes/iri/ixi:/iri/ixi:rw
      - ./volumes/iri/data:/iri/data:rw
      # Use this volume when you want to upgrade the Tangle
      #- ./volumes/iota/tools:/iri/tools:rw
      - /etc/localtime:/etc/localtime:ro
    environment:
     # Use the following for 8GB; comment following line if other setting is chosen 
      - JAVA_MAX_MEMORY=8192m
     # Uncomment the following for 4GB
     # - JAVA_MAX_MEMORY=4096m
      - JAVA_MIN_MEMORY=256m
      - DOCKER_IRI_MONITORING_API_PORT_ENABLE=1
    expose:
      - "5556"
      - "14266"
    ports:
      - "14600:14600/udp"
      - "15600:15600/tcp"
      - "14265:14265"
# To get things started
    command: ["-c", "/iri/conf/iri.ini"]

  nelson.cli:
    image: romansemko/nelson.cli:latest
    container_name: iota_nelson.cli
    hostname: nelson.cli
    restart: unless-stopped
    volumes:
      - ./volumes/nelson/data:/data:rw
      - ./volumes/nelson/config.ini:/home/node/config.ini:ro
      - /etc/localtime:/etc/localtime:ro
    command: "--config /home/node/config.ini"
    ports:
      - "18600:18600"
      - "16600:16600"

  nelson.gui:
    image: romansemko/nelson.gui:latest
    container_name: iota_nelson.gui
    hostname: nelson.gui
    network_mode: "host"
    restart: unless-stopped
    ports:
      - "5000:5000"

  field.cli:
    image: romansemko/field.cli:latest
    container_name: iota_field.cli
    hostname: field.cli
    restart: unless-stopped
    volumes:
      - ./volumes/field/config.ini:/usr/src/field/config.ini:ro
      - ./volumes/field/root:/root
      - /etc/localtime:/etc/localtime:ro
    command: "--config /usr/src/field/config.ini"
    ports:
      - "21310:21310"

  prometheus:
    image: prom/prometheus:latest
    container_name: iota_prometheus
    hostname: prometheus
    restart: unless-stopped
    volumes:
      - ./volumes/prometheus/data:/data
      - ./volumes/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml
      - /etc/localtime:/etc/localtime:ro
    command:
      - "--config.file=/etc/prometheus/prometheus.yml"
      - "--storage.tsdb.path=/data"
      - "--web.console.libraries=/usr/share/prometheus/console_libraries"
      - "--web.console.templates=/usr/share/prometheus/consoles"
    expose:
      - "9090"
    links:
      - iota-prom-exporter:iota-prom-exporter
      - node-exporter:node-exporter

  iota-prom-exporter:
    image: bambash/iota-prom-exporter:latest
    container_name: iota_prom-iotaexp
    hostname: iotape
    restart: on-failure
    expose:
      - "9311"
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ./volumes/ipe/config.js:/exporter/config.js

  node-exporter:
    image: prom/node-exporter:latest
    container_name: iota_prom-nodeexp
    hostname: node-exporter
    restart: unless-stopped
    logging:
        driver: none
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
      - /etc/localtime:/etc/localtime:ro
    command:
      - "--path.procfs=/host/proc"
      - "--path.sysfs=/host/sys"
      - --collector.filesystem.ignored-mount-points
      - "^/(sys|proc|dev|host|etc|rootfs/var/lib/docker/containers|rootfs/var/lib/docker/overlay2|rootfs/run/docker/netns|rootfs/var/lib/docker/aufs|/rootfs/sys/kernel/debug/tracing)($$|/)"
    expose:
       - "9100"
       
  grafana:
    image: grafana/grafana:latest
    container_name: iota_grafana
    restart: on-failure
    ports:
      - "8000:3000"
    user: "root"
    volumes:
      - ./volumes/grafana:/var/lib/grafana
      - /etc/localtime:/etc/localtime:ro
    environment:
      - GF_SERVER_PROTOCOL=http
      - GF_PATHS_PROVISIONING=/var/lib/grafana/provisioning
    links:
      - prometheus:prometheus
